class Arbol {
    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        //Se coloca el nivel que desea buscar
        this.nivel = 5;
        this.busquedaElemento;
        this.buscarNodos = [];
        this.caminoNodo = '';
        this.sumaDeCaminos = 0;
    }

    agregarNodoPadre() {
        var nodo = new Nodo(null, null, "36",36);
        return nodo;
    }

    agregarNodo(nodoPadre, posicion, nombre, valor) {
        var nodo = new Nodo(nodoPadre, posicion, nombre, valor);
        return nodo;
    }

    verificarNivelHijos(nodo) {

        if (nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if (nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if (nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;
    }
    buscarValor(buscarElemento,nodo){

        if(nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        return this.busquedaElemento;
    }
    buscarCaminoNodo(nodo) {
        if (nodo.padre != null) {
            this.caminoNodo = this.caminoNodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre+' '+this.caminoNodo;
    }

    sumarCaminoNodo(nodo) {
        if (nodo.padre != null) {
            this.sumaDeCaminos = this.sumaDeCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumaDeCaminos;
    }
}