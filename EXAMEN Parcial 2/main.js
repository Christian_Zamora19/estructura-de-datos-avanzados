//Se ingresa el valor del nodo para buscar
var busqueda = 7;

var arbol = new Arbol();

arbol.nodoPadre.hI = arbol.agregarNodo(arbol.nodoPadre,"Hijo Izquierda","16",16);
arbol.nodoPadre.hD = arbol.agregarNodo(arbol.nodoPadre,"Hijo Derecha","20",20);
//izquierdo
arbol.nodoPadre.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI,"Hijo Izquierda","8",8);
arbol.nodoPadre.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hI,"Hijo Derecha","8",8);
//Derecho
arbol.nodoPadre.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD,"Hijo Izquierda","8",8);
arbol.nodoPadre.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD,"Hijo Derecha","12",12);
//Izquierdo
arbol.nodoPadre.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hijo Izquierda","e",4);
arbol.nodoPadre.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hijo Derecha","4",4);
//
arbol.nodoPadre.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hijo Izquierda","a",4);
arbol.nodoPadre.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hijo Derecha","4",4);
//Derecho
arbol.nodoPadre.hD.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI,"Hijo Izquierda","4",4);
arbol.nodoPadre.hD.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI,"Hijo Derecha","4",4);
//
arbol.nodoPadre.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD,"Hijo Izquierda","5",5);
arbol.nodoPadre.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD,"Hijo Derecha"," ",7);
//Izquierdo
arbol.nodoPadre.hI.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD,"Hijo Izquierda","n",2);
arbol.nodoPadre.hI.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD,"Hijo Derecha","2",2);
//
arbol.nodoPadre.hI.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD,"Hijo Izquierda","t",2);
arbol.nodoPadre.hI.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD,"Hijo Derecha","m",2);
//DERECHO
arbol.nodoPadre.hD.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI,"Hijo Izquierda","i",2);
arbol.nodoPadre.hD.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI,"Hijo Derecha","2",2);
arbol.nodoPadre.hD.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD,"Hijo Izquierda","h",2);
arbol.nodoPadre.hD.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD,"Hijo Derecha","s",2);
//
arbol.nodoPadre.hD.hD.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hijo Izquierda","2",2);
arbol.nodoPadre.hD.hD.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hijo Derecha","f",3);
//Izquierdo
arbol.nodoPadre.hI.hI.hD.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD,"Hijo Izquierda","o",1);
arbol.nodoPadre.hI.hI.hD.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD,"Hijo Derecha","u",1);
//DERECHO
arbol.nodoPadre.hD.hI.hI.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD,"Hijo Izquierda","X",1);
arbol.nodoPadre.hD.hI.hI.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD,"Hijo Derecha","p",1);
//
arbol.nodoPadre.hD.hD.hI.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI,"Hijo Izquierda","r",1);
arbol.nodoPadre.hD.hD.hI.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI,"Hijo Derecha","l",1);

//********* Arbol completo*********
console.log(arbol);

//*** Buscar Nivel -- Para poner el nivel que se quiere buscar se pone en la clase Arbol
var resulNivel = arbol.verificarNivelHijos(arbol.nodoPadre);
console.log(resulNivel);

//                   "Buscar nodo"  Para su busqueda se tiene que ir arriba ^ y colocar el valor
var busqueda = arbol.buscarValor(busqueda,arbol.nodoPadre);
console.log(busqueda);

//*** Busca camino
var caminoNodo = arbol.buscarCaminoNodo(busqueda);
console.log(caminoNodo);

//**Suma del camino
var sumaTotalDelCamino = arbol.sumarCaminoNodo(busqueda);
console.log(sumaTotalDelCamino);



//console.log("Nivel: "+nivel+": "+ buscarNodos);