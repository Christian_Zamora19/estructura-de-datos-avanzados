
var piezas = document.getElementsByClassName('puzzle');

var tamWidh = [236,189,283,189,236,189,236,284,236,189,237,189,192,189,241,189,236,289,243,191];
var tamHeight = [189,189,189,189,189,236,189,284,189,236,189,294,242,294,189,236,189,189,192,236];

for(var i=0;i<piezas.length;i++){
    piezas[i].setAttribute("width", tamWidh[i]);
    piezas[i].setAttribute("height",tamHeight[i]);
    piezas[i].setAttribute("x", Math.floor((Math.random() * 10) + 1));
    piezas[i].setAttribute("y", Math.floor((Math.random() * 700) + 1));
    piezas[i].setAttribute("onmousedown","seleccionarElemento(evt)");
}
var elementSelect = 0;
var currenX = 0;
var currenY = 0;
var currenPosX = 0;
var currenPosY = 0;
var ad = document.getElementById('ad');
function seleccionarElemento(evt){
    ad.play();
    elementSelect = reordenar(evt);
    currenX = evt.clientX;
    currenY = evt.clientY;
    currenPosx = parseFloat(elementSelect.getAttribute('x'));
    currenPosy = parseFloat(elementSelect.getAttribute('y'));
    elementSelect.setAttribute("onmousemove", "moverElemento(evt)");
}
function moverElemento(evt){
    var dx = evt.clientX - currenX;
    var dy = evt.clientY - currenY;
    currenPosx = currenPosx + dx;
    currenPosy = currenPosy + dy;
    elementSelect.setAttribute("x", currenPosx);
    elementSelect.setAttribute("y", currenPosy);
    currenX = evt.clientX;
    currenY = evt.clientY;
    elementSelect.setAttribute("onmouseout","deseleccionarElemento(evt)");
    elementSelect.setAttribute("onmouseup","deseleccionarElemento(evt)");
    iman();
}
function deseleccionarElemento(evt){
    test();
    if(elementSelect != 0){
        elementSelect.removeAttribute("onmousemove");
        elementSelect.removeAttribute("onmouseout");
        elementSelect.removeAttribute("onmouseup");
        elementSelect = 0;
    }
}
var entorno = document.getElementById('entorno');
function reordenar(evt){
    var padre = evt.target.parentNode;
    var clone = padre.cloneNode(true);
    var id = padre.getAttribute("id");
    entorno.removeChild(document.getElementById(id));
    entorno.appendChild(clone);
    return entorno.lastChild.firstChild;
}
var origX = [500,688,830,1064,1205,500,639,827,1064,1252,500,687,874,1065,1200,500,640,827,1064,1250];
var origY = [100,100,100,100,100,241,289,241,289,242,477,420,473,420,475,617,665,665,660,614];
function iman(){
    for(var i=0;i<piezas.length;i++){
        if(Math.abs(currenPosx-origX[i])<15 && Math.abs(currenPosy-origY[i])<15){
            elementSelect.setAttribute("x",origX[i]);
            elementSelect.setAttribute("y",origY[i]);
        }
    }
}
var win = document.getElementById("win");
function test(){
    var bien_ubicada = 0;
    var padres = document.getElementsByClassName('padre');
    for(var i=0;i<piezas.length;i++){
        var posx = parseFloat(padres[i].firstChild.getAttribute("x"));
        var posy = parseFloat(padres[i].firstChild.getAttribute("y"));
        ide = padres[i].getAttribute("id");
        if(origX[ide] == posx && origY[ide] == posy){
            bien_ubicada = bien_ubicada + 1;
        }
    }
    if(bien_ubicada == 20){
        win.play();
        ad.pause();
        alert("Felicicidades, lo has conseguido");
        onclick=location.reload();
    }
}
var timersCount = 0;
var pausa = false;
coutTimers();
function coutTimers(){
    timersCount++;
    var count = 100;
    var counter = setInterval(timer, 1000);
    function timer(){
        if(!pausa){
            count = count -1;
            if (count < 0){
                clearInterval(counter);
                setTimeout(countTimers, 5000);
                return
            }
            document.getElementById("timer").innerHTML = "Tiempo: " + count;
        }
    }
    document.getElementById("counTimers").innerHTML = timersCount;
}
document.getElementById("pausa").addEventListener("click", function () {
    pausa = true;
    ad.pause();
});
document.getElementById("play").addEventListener("click", function () {
    pausa = false;
    ad.play();
});

