//Crear un arreglo bidimencional a traves de la informacion de mis familiares nucleares
// se colocara la informacion de cada uno de los integrantes (nombres apellidos edad y los gustos
var elementos = [];
elementos.push("Christian");
elementos.push("Zamora");
var animales = [];
animales.push("perro");
animales.push("Saludo");

elementos.push(animales);
console.log(elementos);
console.log(elementos[2][1]);

//Crear un arreglo bidimencional a traves de la informacion de mis familiares nucleares
// se colocara la informacion de cada uno de los integrantes (nombres apellidos edad altura nacionalidad y los gustos
//PRIMERA FORMA
 var familia = [];
 var persona1 = [];
 var persona2 = [];
 var persona3 = [];
 var persona4 = [];
 var persona5 = [];
 persona1.push("Christian");
 persona1.push("Zamora");
 persona1.push("18");
 persona1.push("1.79");
 persona1.push("Mexicano");

persona2.push("Carlos");
persona2.push("Arreola");
persona2.push("21");
persona2.push("1.75");
persona2.push("Mexicano");

persona3.push("Wendy");
persona3.push("Arreola");
persona3.push("13");
persona3.push("1.54");
persona3.push("Mexicana");

persona4.push("Rosa");
persona4.push("Martinez");
persona4.push("51");
persona4.push("1.58");
persona4.push("Mexicana");

persona5.push("Juan");
persona5.push("Ramos");
persona5.push("50");
persona5.push("1.75");
persona5.push("Mexicano");

var gustosChristian = [];
var gustosCarlos = [];
var gustosWendy = [];
var gustosRosa = [];
var gustosJuan = [];

gustosChristian.push("Calistenia");
gustosChristian.push("Street Workout");
gustosChristian.push("Basquetbol");
gustosChristian.push("Programar");
gustosChristian.push("Comer");

gustosCarlos.push("Tecnologia");
gustosCarlos.push("Palomitas");
gustosCarlos.push("Leer");
gustosCarlos.push("Ingles");
gustosCarlos.push("Aprender");

gustosWendy.push("Caricaturas");
gustosWendy.push("TikTok");
gustosWendy.push("Acostarse");
gustosWendy.push("Dulces");
gustosWendy.push("Musica");

gustosRosa.push("Cosinar");
gustosRosa.push("Camiar");
gustosRosa.push("Limpieza");
gustosRosa.push("Arreglarse");
gustosRosa.push("Chocolate");

gustosJuan.push("Tarabajar");
gustosJuan.push("Carros");
gustosJuan.push("Reparar");
gustosJuan.push("Visitar");
gustosJuan.push("Viajar");

persona1.push(gustosChristian);
persona2.push(gustosCarlos);
persona3.push(gustosWendy);
persona4.push(gustosRosa);
persona5.push(gustosJuan);

familia.push(persona1,persona2,persona3,persona4,persona5);
console.log(familia);

// SEGUNDA FORMA
var Familia =
    {
     "Persona 1":
         {
          "Nombre" : "Armando",
          "Apellidos" : "Zamora Arreola",
          "Gustos" :
              {
               "Musica":"Electronica",
               "Comida":"Enmoladas",
               "Deportes":"Calistenia",
               "Animales":"Lobos",
               "Videos":"Street Workout",
              }
         },
     "Persona 2":
         {
          "Nombre" : "Jasdana",
          "Apellidos" : "Aparicio Chavez",
          "Gustos" :
              {
               "Animaless":"Gatos",
               "Ropa":"Mini Faldas",
               "Celular":"iphone",
               "Bailes":"Baladas",
               "Dulces":"Chocolate",
              }
         }
    };
console.log(Familia);