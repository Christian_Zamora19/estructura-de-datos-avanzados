class Nodo {
    constructor(padre = null,nivel, posicion,nombre,valor) {
        this.padre = padre;
        this.nivel = nivel;
        this.posicion = posicion;
        this.nombre = nombre;
        this.valor= valor;
        this.caracteristicas ={
            "Persona 1":
                {
                    "Nombre" : "Armando",
                    "Apellidos" : "Zamora Arreola",
                    "Gustos" :
                        {
                            "Musica":"Electronica",
                            "Comida":"Enmoladas",
                            "Deportes":"Calistenia",
                            "Animales":"Lobos",
                            "Videos":"Street Workout",
                        }
                },
            "Persona 2":
                {
                    "Nombre" : "Jasdana",
                    "Apellidos" : "Aparicio Chavez",
                    "Gustos" :
                        {
                            "Animaless":"Gatos",
                            "Ropa":"Mini Faldas",
                            "Celular":"iphone",
                            "Bailes":"Baladas",
                            "Dulces":"Chocolate",
                        }
                }
        }
        if(padre == null)
            this.nivel = 0;
        else
            this.nivel = padre.nivel + 1;
    }
}