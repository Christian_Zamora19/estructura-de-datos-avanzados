class Arbol {
    constructor() {
        //this.nodoPadre = this.NodoPadre();
        //Se coloca el nivel que desea buscar
        //this.nivel = 5;
        this.nodoPadre = "";
        this.nodos = [];
        //this.busquedaElemento;
        //this.buscarNodos = [];
        // this.caminoNodo = '';
        // this.sumaDeCaminos = 0;
    }

    NodoPadre(padre,nivel, posicion, nombre, valor) {
        let nodo = new Nodo(padre,nivel, posicion, nombre, valor);
        this.nodos.push(nodo);
        return nodo;
    }

    Nodo(padre,nivel, posicion, nombre, valor) {
        var nodo = new Nodo(padre,nivel, posicion, nombre, valor);
        this.nodos.push(nodo);
        return nodo;
    }

    /*verificarNivelHijos(nodo) {

        if (nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if (nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if (nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;
    }
    buscarValor(buscarElemento,nodo){

        if(nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento,nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento,nodo.hD);

        return this.busquedaElemento;
    }
    buscarCaminoNodo(nodo) {
        if (nodo.padre != null) {
            this.caminoNodo = this.caminoNodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre+' '+this.caminoNodo;
    }

    sumarCaminoNodo(nodo) {
        if (nodo.padre != null) {
            this.sumaDeCaminos = this.sumaDeCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumaDeCaminos;
    }*/

    buscarNodosPadre(padre){
        let nodosEncontrados = [];
        for(let x=0; x < this.nodos; x++){
            if(this.nodos[x].padre == padre)
                nodosEncontrados.push(this.nodos[x]);
        }
        return nodosEncontrados;
    }

    agregarNodo(valor, nombre){
        let nodo = new Nodo(null, null, null,valor,nombre);
        if(this.nodos.length == 1){
            nodo.Padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel +1;
            if(nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'Hizq';
            else
                nodo.posicion = 'Dere';
            this.nodos.push(nodo);
        }

    }
}