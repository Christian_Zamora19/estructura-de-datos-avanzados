class Nodo {
    constructor(padre = null,nivel, posicion,nombre,valor) {
        this.padre = padre;
        this.nivel = nivel;
        this.posicion = posicion;
        this.nombre = nombre;
        this.valor= valor;
        if(padre == null)
            this.nivel = 0;
        else
            this.nivel = padre.nivel + 1;
    }
}