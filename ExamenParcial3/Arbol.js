class Arbol {
    constructor() {

        this.nodoPadre = "";
        this.nodos = [];

    }

    NodoPadre(padre, nivel, posicion, nombre, valor) {
        let nodo = new Nodo(padre, nivel, posicion, nombre, valor);
        this.nodos.push(nodo);
        return nodo;
    }

    Nodo(padre,nivel, posicion, nombre, valor) {
        var nodo = new Nodo(padre,nivel, posicion, nombre, valor);
        this.nodos.push(nodo);
        return nodo;
    }

   /* buscarNodosPadre(padre){
        let nodosEncontrados = [];
        for(let x=0; x < this.nodos; x++){
            if(this.nodos[x].padre == padre)
                nodosEncontrados.push(this.nodos[x]);
        }
        return nodosEncontrados;
    }*/

    agregarNodo(valor, nombre) {
        let nodo = new Nodo(null, null, null, valor, nombre);
        if (this.nodos.length == 1) {
            nodo.Padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'Hizq';
            else
                nodo.posicion = 'Dere';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 2) {
            nodo.Padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'Hizq';
            else
                nodo.posicion = 'Dere';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 3) {
            nodo.Padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'Hizq';
            else
                nodo.posicion = 'Dere';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 4) {
            nodo.Padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'Hizq';
            else
                nodo.posicion = 'Dere';
            this.nodos.push(nodo);
        }
        else if (this.nodos.length == 5) {
            nodo.Padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1;
            if (nodo.valor < this.nodoPadre.valor)
                nodo.posicion = 'Hizq';
            else
                nodo.posicion = 'Dere';
            this.nodos.push(nodo);
        }

    }
}