//Da la fecha de inicio de la primera fila 
function fec_inicio(){
    var d1 = new Date();
    var fe_ini = d1.getFullYear() +"/"+ (d1.getMonth()+1) +"/"+ d1.getDate();

    return fe_ini;
}

//Da la fecha de termino de la primera fila 
function fec_fin(){
    var d2 = new Date();
    d2.setMonth(d2.getMonth() + 1);
    var fe_fin = d2.getFullYear() +"/"+ (d2.getMonth()+1) +"/"+ d2.getDate();

    return fe_fin;
}

//VCalcula los dias entre los dos meses 
function dia(fe_ini,fe_fin){

    var dia1 = new Date(fe_ini); 
    var dia2 = new Date(fe_fin);
    
    var residuo= Math.abs(dia2-dia1);
    dias = residuo/(1000 * 3600 * 24)

    return Math.round(dias);
}


//Funcion principal para llamar todos los metodos
function principal(){
    //Varbles de entrada del usuarios 
    var _importe = document.getElementById("importe").value;
    var _plazo = document.getElementById("plazos").value;
    var _taza = document.getElementById("taza").value;
    
    //Metodos y variables para calcular 
    var fe_ini = fec_inicio();
    var fe_fin = fec_fin();
    var dias = dia(fe_ini,fe_fin);
    var _amort = _importe / _plazo ;
    var _porcen = _taza/100;
    var _inte = _importe * _porcen / 360 * dias;
    var _iva = _inte * 0.16; 
    var _flujo = _amort + _inte + _iva;

//Agregar fila y columnas a la tabla 
    var fila="<tr><td>"+1+"</td><td>"+fe_ini+"</td><td>"+fe_fin+"</td><td>"+dias+"</td><td></td><td>"+_importe+"</td><td>"+_amort+"</td><td>"+_inte+"</td><td>"+_iva+"</td><td>"+_flujo+"</td></tr>";

//Agregar la informacion a traves del boton
    var btn = document.createElement("TR");
   	btn.innerHTML=fila;
    document.getElementById("tablita").appendChild(btn);
     //Llamar la continuacion de la tabla   
     tabla_2(_plazo, _importe,_amort,_porcen);

}

//Da la fecha de inicio de la primera fila 
function fec_inicio2(){

    var n = new Date();
    n.setMonth(n.getMonth() + 1)

    fecha1 = n.getFullYear() +"/"+ (n.getMonth()+1) +"/"+ n.getDate();

    return fecha1;

}

function tabla_2(_plazo, _importe, _amort, _porcen){
    //Constructores de fecha 
    var n = new Date();
    var n2 = new Date();
    n2.setMonth(n2.getMonth() + 1)
    var g = new Date();
    var h = new Date();
    g.setMonth(g.getMonth() + 1)
    h.setMonth(h.getMonth() + 2)
    //Variable saldo
    var _saldo = _importe;

//Repeticion del proceso de acuerdo al plazo
for (var i = 2; i <= _plazo; i++) { 
        //Formato de fecha 
        n.setMonth(n.getMonth() + 1)
        n2.setMonth(n2.getMonth() + 1)
        fecha1 = n.getFullYear() +"/"+ (n.getMonth()+1) +"/"+ n.getDate();
        fecha2 = n2.getFullYear() +"/"+ (n2.getMonth()+1) +"/"+ n2.getDate();
        f1 = new Date(n.getFullYear() +"/"+ (n.getMonth()+1) +"/"+ n.getDate());
        f2 = new Date(n2.getFullYear() +"/"+ (n2.getMonth()+1) +"/"+ n2.getDate());
        
        //Calculos necesarios 
        var difference= Math.abs(f2-f1);
        days = difference/(1000 * 3600 * 24)
        var _amort = _importe / _plazo ;
        _saldo = _saldo - (_amort);
        var _inte = _saldo * _porcen / 360 * days;
        var _iva = _inte * 0.16; 
        var _flujo = _amort + _inte + _iva;

//Mostrar resultados en la tabla 
        var fila="<tr><td>"+i+"</td><td>"+ fecha1 +"</td><td>"+ fecha2 +"</td><td>"+Math.round(days)+"</td><td></td><td>"+_saldo+"</td><td>"+_amort+"</td><td>"+_inte+"</td><td>"+_iva+"</td><td>"+_flujo+"</td><tr>";
        var btn = document.createElement("TR");
        btn.innerHTML=fila;
     document.getElementById("tablita").appendChild(btn);
    }
}


