class Nodo{
    constructor(tipo,valor,nivel,padre,hijoIZQ= 0,hijoDER= 0) {
        this.tipo = tipo;
        this.valor = valor;
        this.nivel = nivel;
        this.hijoIZQ = hijoIZQ;
        this.hijoDER = hijoDER;

        if(hijoIZQ == 1)
            this.hijoIzquierdo = this.crearHijo("Izq", 0,nivel + 1, this.nivel);
        if(hijoDER == 1)
            this.hijoDerecho = this.crearHijo("Der", 0, nivel + 1, this.nivel);

    }
    crearHijo(tipo,valor,nivel,padre){
        var crearHijo = new Nodo(tipo,valor,nivel,padre);
        return crearHijo;
    }
}