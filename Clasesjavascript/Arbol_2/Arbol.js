class Arbol{
    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
    }
    agregarNodoPadre(){
        var nodo = new Nodo("Izquierdo", true,0,0,1,1,2,2,3,3);
        return nodo;
    }
    agregarNodo(nodoPadre,Posicion,valor,nivel) {
        var nodo = new Nodo(nodoPadre,Posicion, valor, nivel);
        return nodo;
    }
}