class Nodo{
    constructor(Posicion,valor,nivel,padre,hijoIZQ= 1,hijoDER= 1) {
        this.Posicion = Posicion;
        this.valor = valor;
        this.nivel = nivel;
        this.hijoIZQ = hijoIZQ;
        this.hijoDER = hijoDER;

        if(hijoIZQ == null)
            this.hijoIzquierdo = this.creahijo("Izquierdo", 1,nivel + 1, this.nivel);

        if(hijoDER == null)
            this.hijoDerecho = this.creahijo("Derecho", 1, nivel + 1, this.nivel);
    }
    creahijo(Posicion,valor,nivel,padre){
        var creahijo = new Nodo(Posicion,valor,nivel,padre);
        return creahijo;
    }
}