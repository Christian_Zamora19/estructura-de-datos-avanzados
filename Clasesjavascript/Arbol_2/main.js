var arbol = new Arbol();
arbol.nodoPadre.hijoDER = arbol.agregarNodo(arbol.nodoPadre,"Derecho",0,1);
arbol.nodoPadre.hijoIZQ = arbol.agregarNodo(arbol.nodoPadre,"Izquierdo",0,1);

arbol.nodoPadre.hijoIZQ.hijoDER = arbol.agregarNodo(arbol.nodoPadre.hijoDER,"Derecho",1,2);
arbol.nodoPadre.hijoIZQ.hijoIZQ = arbol.agregarNodo(arbol.nodoPadre.hijoIZQ,"Izquierdo",1,2);

arbol.nodoPadre.hijoIZQ.hijoDER.hijoIZQ = arbol.agregarNodo(arbol.nodoPadre.hijoDER.hijoDER,"Derecho",2,3);
arbol.nodoPadre.hijoIZQ.hijoDER.hijoDER = arbol.agregarNodo(arbol.nodoPadre.hijoIZQ.hijoDER,"Izquierdo",2,3);
console.log(arbol);

